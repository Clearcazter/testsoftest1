//
//  MusicViewController.swift
//  TestPrueba
//
//  Created by Gerardo Castillo on 11/10/18.
//  Copyright © 2018 GerardoCastillo. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController {
    var songs:[Song] = []
    
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        donwloadSong()
        setUpSearchBar()

        // Do any additional setup after loading the view.
    }
    func donwloadSong(){
        Music.fetchSongs { (result: [Song]) in
            self.songs = result //manda llamar la canciones
            DispatchQueue.main.async {
                self.tableView.reloadData() //va a recargar la tabla
            }
        }
    }
    //Configracion del searchbar

    func setUpSearchBar () {
        //Necesitamso un controlador de busqueda
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "search Song"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    
    func donwloadSongsByName(nameSong: String) {
        Music.fetchSongs(songName: nameSong) { (results :[Song]) in
            self.songs = results
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        }
    

}
extension MusicViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "musiCell", for: indexPath)
        cell.textLabel?.text = songs[indexPath.row].name
        return cell
    }
}

extension MusicViewController: UISearchResultsUpdating {
    //eSTE PROTOCOLO se manda a llamar cuando empecemos a escribir texto
    func updateSearchResults(for searchController: UISearchController) {
    donwloadSongsByName(nameSong: searchController.searchBar.text!)
    }
    
    
}
